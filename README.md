# usb_nus_c

Firmware for the [nRF52840 dongle](https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dongle) or the [nRF52840-DK](https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dk) that make a bridge between an USB serial and a BLE central using the Nordic UART Service (NUS) client.
It's based on the ble_app_uart_c example from Nordics and try to have a better modularity by putting the BLE and USB parts on separated files.
For the other side of the communication, the project [ble_app_uart](https://gitlab.com/nbr-reds/ble_app_uart) can be used.

## Programmation

- Plug the nRF52840 dongle to your PC
- Open Programmer from [nRF Connect for Desktop](https://www.nordicsemi.com/Products/Development-tools/nrf-connect-for-desktop)
- Select the device
- Add file -> Browse ... -> bin/s140_nrf52_7.2.0_softdevice.hex and bin/usb_nus_c_pca10059.hex
- Write

## IDE installation

Follow the instructions found in the [REDS blog](https://blog.reds.ch/?p=1548).

## Build configurations

- pca10056: configuration for the nRF52840-DK
- pca10059: configuration for the nRF52840 dongle
- flash_softdevice: for programming the softdevice, need to be done one time before debugging the other configuration