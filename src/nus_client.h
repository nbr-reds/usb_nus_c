/**
 ******************************************************************************
 * @file    nus_client.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    13-December-2022
 * @brief   Nordic UART Service client (central)
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef NUS_CLIENT_H
#define NUS_CLIENT_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Exported types ------------------------------------------------------------*/

typedef enum {
    NUS_CLIENT_EVENT_CONNECTED,
    NUS_CLIENT_EVENT_DISCONNECTED,
    NUS_CLIENT_EVENT_RX_DATA,
} nus_client_event_t;

typedef void (*nus_client_event_function_t) (nus_client_event_t event, const uint8_t* data, uint16_t length);

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the NUS client
 * @param nus_client_event_function the function to call on event
 */
void nus_client_init(nus_client_event_function_t nus_client_event_function);

/**
 * Transmit data
 * @param data the data to sent
 * @param length the length of the data
 */
void nus_client_tx(const uint8_t* data, size_t length);

/**
 * Enable or disable the BLE
 * @param enable true to enable the BLE
 */
uint16_t nus_client_get_max_len(void);

#endif
