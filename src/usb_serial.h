/**
 ******************************************************************************
 * @file    usb_serial.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-December-2022
 * @brief   USB serial comm
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef USB_SERIAL_H
#define USB_SERIAL_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

/* Exported types ------------------------------------------------------------*/

typedef void (*usb_serial_rx_function_t)(uint8_t* data, size_t length);

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the USB serial
 * @param usb_serial_rx_function the function to call when data are received
 */
void usb_serial_init(usb_serial_rx_function_t usb_serial_rx_function);

/**
 * Function to call repeatedly to process USB event
 */
void usb_serial_tick(void);

/**
 * Transmit data
 * @param data the data to sent
 * @param length the length of the data
 */
void usb_serial_tx(const uint8_t* data, size_t length);

#endif
