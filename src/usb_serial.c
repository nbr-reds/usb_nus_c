/**
 ******************************************************************************
 * @file    usb_serial.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-December-2022
 * @brief   USB serial comm
 *          Most code is from nrf5_sdk_17.1.0_ddde560\examples\peripheral\usbd_cdc_acm
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/**
 * Copyright (c) 2016 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include "nrf.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_usbd.h"
#include "nrf_gpio.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_util.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"

#define NRF_LOG_MODULE_NAME usb_serial
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#include "usb_serial.h"

/* Private define ------------------------------------------------------------*/

/**
 * @brief Enable power USB detection
 *
 * Configure if example supports USB port connection
 */
#ifndef USBD_POWER_DETECTION
#define USBD_POWER_DETECTION true
#endif

#define CDC_ACM_COMM_INTERFACE  0
#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE  1
#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1

#define RX_BUFFER_SIZE          NRF_DRV_USBD_EPSIZE

/* Private variables ---------------------------------------------------------*/

static void process_read(app_usbd_cdc_acm_t const* p_cdc_acm);
static void start_new_read(app_usbd_cdc_acm_t const* p_cdc_acm);
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
        app_usbd_cdc_acm_user_event_t event);

/**
 * @brief CDC_ACM class instance
 * */
APP_USBD_CDC_ACM_GLOBAL_DEF(m_app_cdc_acm,
        cdc_acm_user_ev_handler,
        CDC_ACM_COMM_INTERFACE,
        CDC_ACM_DATA_INTERFACE,
        CDC_ACM_COMM_EPIN,
        CDC_ACM_DATA_EPIN,
        CDC_ACM_DATA_EPOUT,
        APP_USBD_CDC_COMM_PROTOCOL_AT_V250
);

static uint8_t m_rx_buffer[RX_BUFFER_SIZE];

static usb_serial_rx_function_t rx_function;

/* Private functions declaration ---------------------------------------------*/

static void usbd_user_ev_handler(app_usbd_event_type_t event);

/* Public functions ----------------------------------------------------------*/

void usb_serial_init(usb_serial_rx_function_t usb_serial_rx_function)
{
    ret_code_t ret;
    static const app_usbd_config_t usbd_config = {
            .ev_state_proc = usbd_user_ev_handler
    };

    rx_function = usb_serial_rx_function;

    app_usbd_serial_num_generate();

    ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    ret = app_usbd_init(&usbd_config);
    APP_ERROR_CHECK(ret);

    app_usbd_class_inst_t const * class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_app_cdc_acm);
    ret = app_usbd_class_append(class_cdc_acm);
    APP_ERROR_CHECK(ret);

    if (USBD_POWER_DETECTION) {
        ret = app_usbd_power_events_enable();
        APP_ERROR_CHECK(ret);
    } else {
        NRF_LOG_INFO("No USB power detection enabled\r\nStarting USB now");

        app_usbd_enable();
        app_usbd_start();
    }

}

void usb_serial_tick(void)
{
    while (app_usbd_event_queue_process());
}

void usb_serial_tx(const uint8_t* data, size_t length)
{
    app_usbd_cdc_acm_write(&m_app_cdc_acm, data, length);
}

/* Private functions implementation ------------------------------------------*/

static void process_read(app_usbd_cdc_acm_t const* p_cdc_acm)
{
    if (rx_function != NULL) {
        rx_function(m_rx_buffer, app_usbd_cdc_acm_rx_size(p_cdc_acm));
    }
}

static void start_new_read(app_usbd_cdc_acm_t const* p_cdc_acm)
{
    while (true) {
        ret_code_t ret = app_usbd_cdc_acm_read_any(p_cdc_acm,
                m_rx_buffer,
                RX_BUFFER_SIZE);
        if (ret == NRF_SUCCESS) {
            process_read(p_cdc_acm);
        } else if (ret == NRF_ERROR_IO_PENDING) {
            break;
        } else {
            NRF_LOG_ERROR("app_usbd_cdc_acm_read_any() error: %i", ret);
            break;
        }
    }
}

/**
 * @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t (headphones)
 * */
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
        app_usbd_cdc_acm_user_event_t event)
{
    app_usbd_cdc_acm_t const* p_cdc_acm = app_usbd_cdc_acm_class_get(p_inst);

    switch (event)
    {
    case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
        start_new_read(p_cdc_acm);
        break;
    case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
        break;
    case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
        break;
    case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
        process_read(p_cdc_acm);
        start_new_read(p_cdc_acm);
        break;
    default:
        break;
    }
}

static void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch (event)
    {
    case APP_USBD_EVT_DRV_SUSPEND:
        break;
    case APP_USBD_EVT_DRV_RESUME:
        break;
    case APP_USBD_EVT_STARTED:
        break;
    case APP_USBD_EVT_STOPPED:
        app_usbd_disable();
        break;
    case APP_USBD_EVT_POWER_DETECTED:
        NRF_LOG_INFO("USB power detected");

        if (!nrf_drv_usbd_is_enabled())
        {
            app_usbd_enable();
        }
        break;
    case APP_USBD_EVT_POWER_REMOVED:
        NRF_LOG_INFO("USB power removed");
        app_usbd_stop();
        break;
    case APP_USBD_EVT_POWER_READY:
        NRF_LOG_INFO("USB ready");
        app_usbd_start();
        break;
    default:
        break;
    }
}
