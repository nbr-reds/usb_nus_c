/**
 ******************************************************************************
 * @file    uart.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-December-2022
 * @brief   UART
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include "app_uart.h"
#include "boards.h"

#define NRF_LOG_MODULE_NAME uart
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#include "uart.h"

/* Private define ------------------------------------------------------------*/

#define UART_TX_BUF_SIZE        256
#define UART_RX_BUF_SIZE        256

/* Private variables ---------------------------------------------------------*/

static uart_rx_function_t rx_function;
static uint8_t rx_buffer[UART_RX_BUF_SIZE];

/* Private functions declaration ---------------------------------------------*/

static void uart_event_handle(app_uart_evt_t * p_event);

/* Exported functions --------------------------------------------------------*/

void uart_init(uart_rx_function_t uart_rx_function)
{
    ret_code_t err_code;
    rx_function = uart_rx_function;

    app_uart_comm_params_t const comm_params =
    {
            .rx_pin_no    = RX_PIN_NUMBER,
            .tx_pin_no    = TX_PIN_NUMBER,
            .rts_pin_no   = RTS_PIN_NUMBER,
            .cts_pin_no   = CTS_PIN_NUMBER,
            .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
            .use_parity   = false,
            .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
            UART_RX_BUF_SIZE,
            UART_TX_BUF_SIZE,
            uart_event_handle,
            APP_IRQ_PRIORITY_LOWEST,
            err_code);

    APP_ERROR_CHECK(err_code);
}

void uart_tx(const uint8_t* data, size_t length)
{
    ret_code_t ret_val;
    for (uint32_t i = 0; i < length; i++)
    {
        do
        {
            ret_val = app_uart_put(data[i]);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}

/* Private functions implementation ------------------------------------------*/

/**@brief   Function for handling app_uart events.
 *
 * @details This function receives a single character from the app_uart module and appends it to
 *          a string. The string is sent over BLE when the last character received is a
 *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
 */
static void uart_event_handle(app_uart_evt_t * p_event)
{
    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&rx_buffer[0]));
            if (rx_function != NULL) {
                rx_function(rx_buffer, 1);
            }
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
